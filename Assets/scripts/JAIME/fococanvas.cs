﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class fococanvas : MonoBehaviour
{
    public Transform verificafoco;
    public Transform foco;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (verificafoco.gameObject.GetComponent<Light>().enabled == true)
        {
            foco.gameObject.GetComponent<Image>().enabled = true;
        }
        else
        {
            foco.gameObject.GetComponent<Image>().enabled = false;
        }
    }
}
