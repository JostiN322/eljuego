﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas_Scary : MonoBehaviour
{
    public GameObject panel;
    Animator anim_panel;
    void Start()
    {
        anim_panel = panel.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            anim_panel.SetBool("ScaryMask", true);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            anim_panel.SetBool("ScaryMask", false);
        }
    }
}
