﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class prenderlz : MonoBehaviour
{

    public List<GameObject> luces_del_cuarto;//lista de luces
    public GameObject interruptor;
    public Transform prende;//musica
    public bool isTrigger;
    void Start()    
    {

        prende.gameObject.SetActive(false);
       // luz.gameObject.SetActive(false);
    }

    
    void Update()
    {
        if (isTrigger == true )
        {
            if (Input.GetMouseButtonDown(0))
            {
                for (int i = 0; i < luces_del_cuarto.Count; i++)
                {
                    luces_del_cuarto[i].GetComponent<Light>().enabled = true;// activa a todas
                }
            }                    
            //no se coloca apagar, ya que lo que se necesita es que las luces siempre estén apagadas
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        isTrigger = true;
    }
    private void OnTriggerExit(Collider other)
    {
        isTrigger = false;
    }
}
