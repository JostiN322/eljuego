﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nodos : MonoBehaviour
{
    [Header("--LUCES Y NODOS_HERMANOS--")]
    public List<Nodos> nodos_adyacentes;
    public List<GameObject> luces_del_nodo;//LUCES QUE CONTIENE EL NODO

    [Header("--NO_MOFICAR--")]
    public float distancia_con_fantasma;
    public int cantidad_de_adyacentes;
    public int cantidad_de_luces;
    public float[] distancia_luces_fantasma;

    private void Start()
    {
        cantidad_de_adyacentes = nodos_adyacentes.Count;
        cantidad_de_luces = luces_del_nodo.Count;
        distancia_luces_fantasma = new float[cantidad_de_luces];
    }

}
