﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class barra_estamina : MonoBehaviour
{
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController pedro;

    public Velocidad velcidad;

    public Image barImage;

    public float gastando;

    private void Awake()
    {
        barImage = transform.Find("bar").GetComponent<Image>();

        velcidad = new Velocidad();
     
    }

    private void Update()
    {
        
        if (Input.GetKey(KeyCode.LeftShift))
        {
            velcidad.TrySpendVelocidad(gastando);
        }

        velcidad.Update();
       // Debug.Log(velcidad.GetVelocityNormalized());
        pedro.energia = velcidad.GetVelocityNormalized();

        barImage.fillAmount = velcidad.GetVelocityNormalized();

    }

}
public class Velocidad {
    public const int Velocidad_Max = 100;

    private float veloAmount;
    private float veloRegenAmount;

    public Velocidad()
    {
        veloAmount = 0;
        veloRegenAmount = 17.0f; //regenera mana
    }
    public void Update()
    {
        veloAmount += veloRegenAmount * Time.deltaTime;
        veloAmount = Mathf.Clamp(veloAmount, 0f, Velocidad_Max);
    }

    public void TrySpendVelocidad(float amount)
    {
        if (veloAmount >= amount)
        {
            veloAmount -= amount;
        }     
    }
    
    public float GetVelocityNormalized()
    {
        return veloAmount / Velocidad_Max;
    }
}