﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fantasma : MonoBehaviour
{
    [Header("--MOVIMIENTO--")]
    public Nodos tarjet_grafo_now;
    public Transform tarjet_transform;
    public float velocidad_fantasma;

    [Header("--ATAQUE--")]
    public int coolDown_LucesOff;
    public float rango_MinToLucesOff;
    private float contador_para_apagar_luces;
    private List<Nodos> all_grafos;

    [Header("MIEDO_JUGADOR")]
    public GameObject player;
    public float rango_MinMiedo;
    public AudioSource[] audio_sources;

    void Start()
    {
        for (int i = 0; i < audio_sources.Length; i++)
        {
            audio_sources[i].volume = 0f;
        }
        all_grafos = new List<Nodos>();
        contador_para_apagar_luces = 0;
    }
    void Update()
    {
        if (tarjet_transform != null)
        {
            Moveto();
        }
        Miedo_Jugador();
        contador_para_apagar_luces += Time.deltaTime;
        if (contador_para_apagar_luces > coolDown_LucesOff)
        {
            AttackFantasma();
            contador_para_apagar_luces = 0;
        }
 
    }

    #region "Movimiento"
    void SelectGrafo() 
    {
        ReciclarNodos();

        int num_random = Random.Range(0, tarjet_grafo_now.nodos_adyacentes.Count);
        int index = Mathf.Clamp(num_random, 0, tarjet_grafo_now.nodos_adyacentes.Count);
        tarjet_transform = tarjet_grafo_now.nodos_adyacentes[index].transform;
        tarjet_grafo_now = tarjet_grafo_now.nodos_adyacentes[index];

        NuevosNodos();
    }

    void Moveto() 
    {
        transform.position = Vector3.MoveTowards(transform.position, tarjet_transform.position, velocidad_fantasma * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Fuente_luz")
        {
            SelectGrafo();
        }
    }
    #endregion

    #region "Distancias_Luces"

    void AttackFantasma()
    {
        for (int i = 0; i < all_grafos.Count; i++)
        {
            all_grafos[i].distancia_con_fantasma = Vector3.Distance(gameObject.transform.position, all_grafos[i].transform.position);
            //Debug.Log(all_grafos[i].distancia_con_fantasma);
            if (rango_MinToLucesOff > all_grafos[i].distancia_con_fantasma)
            {
                for (int j = 0; j < all_grafos[i].cantidad_de_luces; j++)
                {
                    all_grafos[i].distancia_luces_fantasma[j] = Vector3.Distance(gameObject.transform.position, all_grafos[i].luces_del_nodo[j].transform.position);

                    //Debug.Log(all_grafos[i].distancia_luces_fantasma[j]);

                    if (rango_MinToLucesOff > all_grafos[i].distancia_luces_fantasma[j])
                    {
                        all_grafos[i].luces_del_nodo[j].gameObject.GetComponent<Light>().enabled = false;
                    }
                }

            }
        }
        
    }
    void NuevosNodos() 
    {
        for (int i = 0; i < tarjet_grafo_now.cantidad_de_adyacentes; i++)
        {
            all_grafos.Add(tarjet_grafo_now.nodos_adyacentes[i]);
        }
        all_grafos.Add(tarjet_grafo_now);
    }

    void ReciclarNodos() 
    {
        all_grafos.Clear();
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        for (int i = 0; i < tarjet_grafo_now.nodos_adyacentes.Count; i++)
        {
            Gizmos.DrawLine(gameObject.transform.position, tarjet_grafo_now.nodos_adyacentes[i].transform.position);
        }
    }

    #endregion

    void Miedo_Jugador() 
    {
        float distance_jugador_fantasma = Vector3.Distance(this.gameObject.transform.position, player.transform.position);
        if (distance_jugador_fantasma < rango_MinMiedo)
        {
            audio_sources[0].volume += 0.001f;
            if (audio_sources[0].volume > 0.2f)
            {
                audio_sources[0].volume = 0.2f;
                if (distance_jugador_fantasma < 100)
                {
                    audio_sources[1].volume += 0.001f;
                    if (audio_sources[1].volume > 0.2f)
                    {
                        audio_sources[1].volume = 0.2f;
                    }
                }
                else
                {
                    audio_sources[1].volume -= 0.001f;
                    if (audio_sources[1].volume <0)
                    {
                        audio_sources[1].volume = 0f;
                    }
                }
            }
        }
        else
        {
            audio_sources[0].volume -= 0.001f;
            if (audio_sources[0].volume < 0)
            {
                audio_sources[0].volume = 0;
            }
        }
        Debug.Log(distance_jugador_fantasma);
    }

}
